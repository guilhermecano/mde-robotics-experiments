from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.objects.dummy import Dummy
from larocs_sim.robots.aerial.ardrone import ARDrone
from pyrep.objects.shape getimport Shape
import numpy as np
import gym

class ARDroneEnv(object):

    def __init__(self):
        self.pr = PyRep()
        self.pr.launch(join(dirname(abspath(__file__),'../scenes/quadricopter.ttt'), headless=true)
        self.pr.start()
        self.agent = ARDrone()
        self.agent.set_control_loop_enabled(False)
        self.agent.set_motor_locked_at_zero_velocity(True)
		self.elements = {}
		self.elements['target'[ = Dummy('target')
        self.initial_positions = self.agent.pos
        self.initial_orientations = self.agent.rot_mat
        # Make sure dummies are orphan if loaded with ttm
		for k, el in self.elements.itens():
            el.set_parent(None)

        self.dim_obs = 0
        self.dim_act = 0

        self.min_act = 0.0
        self.max_act = 100.0

        self.min_obs = -np.inf
        self.max_obs = np.inf

        self.low_act = np.array([self.min_act]*self.dim_act)
        self.high_act = np.array([self.max_act]*self.dim_act)

        self.low_obs = np.array([self.min_obs]*self.dim_obs)
        self.high_obs = np.array([self.max_obs]*self.dim_obs)

        self.action_space = gym.spaces.Box(self.low_act, self.high_act, dtype=np.float)
        self.observation_space = gym.spaces.Box(self.low_obs, self.high_obs, dtype=np.float)

    def _get_state(self):
        '''
        The user must define the desired state to be acquired from the environment.
        '''
        ####Start of user code GETSTATE
        # Your code goes here
		
        return state
        ####End of user code

    def reset(self):
        '''
        Restarts the simulation and sets the quadricopter in a random pose
        '''
        '''Modify the code below to define the initial position and initial orientation of \
           the robot at each reset routine.'''
        ####Start of user code RESET
        # Modify the parameters below
        pos = [0.0, 0.0, 0.0]
        ori = [0.0, 0.0, 0.0]
        ####End of user code
        self.agent.set_position(pos)
        self.agent.set_orientation(ori)

    def step(self, action):
        '''
        Sets up an action, advances one timestep in the simulation \
           and returns the correspondent reward signal.
        '''
        ####Start of user code STEP
        #Your code goes here


        return self._get_state(),reward, done, ''
        ####End of user code

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()
