from larocs_sim.agents.sac import experiment
from larocs_sim.envs.env import env
from rlkit.launchers.launcher_util import setup_logger
from os.path import join, dirname, abspath

if __name__ == "__main__":
    variant = dict(
        algorithm='SAC',
        version='normal',
        replay_buffer_size=int(1000000),
        algorithm_kwargs=dict(
            num_epochs=30,
            num_eval_steps_per_epoch=1000,
            num_trains_per_train_loop=1000,
            num_expl_steps_per_train_loop=1000,
            min_num_steps_before_training=1000,
            max_path_length=1000,
            batch_size=256,
        ),
        trainer_kwargs=dict(
            discount=0.99,
            soft_target_tau=0.005,
            target_update_period=1,
            policy_lr=3.0E-4,
            qf_lr=3.0E-4,
            reward_scale=1,
            use_automatic_entropy_tuning=true,
        ),
    )
    setup_logger('ArDroneRLXtext', variant=variant)
    experiment(variant)
