/**
 */
package mderl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trainer Kwargs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mderl.TrainerKwargs#getDiscount <em>Discount</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#getSoft_target_tau <em>Soft target tau</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#getTarget_update_period <em>Target update period</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#getPolicy_lr <em>Policy lr</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#getQf_lr <em>Qf lr</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#getReward_scale <em>Reward scale</em>}</li>
 *   <li>{@link mderl.TrainerKwargs#isAuto_entropy_tuning <em>Auto entropy tuning</em>}</li>
 * </ul>
 *
 * @see mderl.MderlPackage#getTrainerKwargs()
 * @model
 * @generated
 */
public interface TrainerKwargs extends EObject {
	/**
	 * Returns the value of the '<em><b>Discount</b></em>' attribute.
	 * The default value is <code>"0.99"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discount</em>' attribute.
	 * @see #setDiscount(float)
	 * @see mderl.MderlPackage#getTrainerKwargs_Discount()
	 * @model default="0.99" required="true"
	 * @generated
	 */
	float getDiscount();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getDiscount <em>Discount</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discount</em>' attribute.
	 * @see #getDiscount()
	 * @generated
	 */
	void setDiscount(float value);

	/**
	 * Returns the value of the '<em><b>Soft target tau</b></em>' attribute.
	 * The default value is <code>"0.005"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft target tau</em>' attribute.
	 * @see #setSoft_target_tau(float)
	 * @see mderl.MderlPackage#getTrainerKwargs_Soft_target_tau()
	 * @model default="0.005"
	 * @generated
	 */
	float getSoft_target_tau();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getSoft_target_tau <em>Soft target tau</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Soft target tau</em>' attribute.
	 * @see #getSoft_target_tau()
	 * @generated
	 */
	void setSoft_target_tau(float value);

	/**
	 * Returns the value of the '<em><b>Target update period</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target update period</em>' attribute.
	 * @see #setTarget_update_period(int)
	 * @see mderl.MderlPackage#getTrainerKwargs_Target_update_period()
	 * @model default="1"
	 * @generated
	 */
	int getTarget_update_period();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getTarget_update_period <em>Target update period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target update period</em>' attribute.
	 * @see #getTarget_update_period()
	 * @generated
	 */
	void setTarget_update_period(int value);

	/**
	 * Returns the value of the '<em><b>Policy lr</b></em>' attribute.
	 * The default value is <code>"0.0003"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy lr</em>' attribute.
	 * @see #setPolicy_lr(float)
	 * @see mderl.MderlPackage#getTrainerKwargs_Policy_lr()
	 * @model default="0.0003"
	 * @generated
	 */
	float getPolicy_lr();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getPolicy_lr <em>Policy lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy lr</em>' attribute.
	 * @see #getPolicy_lr()
	 * @generated
	 */
	void setPolicy_lr(float value);

	/**
	 * Returns the value of the '<em><b>Qf lr</b></em>' attribute.
	 * The default value is <code>"0.0003"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qf lr</em>' attribute.
	 * @see #setQf_lr(float)
	 * @see mderl.MderlPackage#getTrainerKwargs_Qf_lr()
	 * @model default="0.0003"
	 * @generated
	 */
	float getQf_lr();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getQf_lr <em>Qf lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qf lr</em>' attribute.
	 * @see #getQf_lr()
	 * @generated
	 */
	void setQf_lr(float value);

	/**
	 * Returns the value of the '<em><b>Reward scale</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reward scale</em>' attribute.
	 * @see #setReward_scale(int)
	 * @see mderl.MderlPackage#getTrainerKwargs_Reward_scale()
	 * @model default="1"
	 * @generated
	 */
	int getReward_scale();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#getReward_scale <em>Reward scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reward scale</em>' attribute.
	 * @see #getReward_scale()
	 * @generated
	 */
	void setReward_scale(int value);

	/**
	 * Returns the value of the '<em><b>Auto entropy tuning</b></em>' attribute.
	 * The default value is <code>"True"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto entropy tuning</em>' attribute.
	 * @see #setAuto_entropy_tuning(boolean)
	 * @see mderl.MderlPackage#getTrainerKwargs_Auto_entropy_tuning()
	 * @model default="True"
	 * @generated
	 */
	boolean isAuto_entropy_tuning();

	/**
	 * Sets the value of the '{@link mderl.TrainerKwargs#isAuto_entropy_tuning <em>Auto entropy tuning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto entropy tuning</em>' attribute.
	 * @see #isAuto_entropy_tuning()
	 * @generated
	 */
	void setAuto_entropy_tuning(boolean value);

} // TrainerKwargs
