/**
 */
package mderl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RLK Algorithm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mderl.RLKAlgorithm#getPolicy <em>Policy</em>}</li>
 *   <li>{@link mderl.RLKAlgorithm#getConfig <em>Config</em>}</li>
 * </ul>
 *
 * @see mderl.MderlPackage#getRLKAlgorithm()
 * @model
 * @generated
 */
public interface RLKAlgorithm extends Model {
	/**
	 * Returns the value of the '<em><b>Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy</em>' attribute.
	 * @see #setPolicy(String)
	 * @see mderl.MderlPackage#getRLKAlgorithm_Policy()
	 * @model required="true"
	 * @generated
	 */
	String getPolicy();

	/**
	 * Sets the value of the '{@link mderl.RLKAlgorithm#getPolicy <em>Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy</em>' attribute.
	 * @see #getPolicy()
	 * @generated
	 */
	void setPolicy(String value);

	/**
	 * Returns the value of the '<em><b>Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Config</em>' containment reference.
	 * @see #setConfig(RLKConfig)
	 * @see mderl.MderlPackage#getRLKAlgorithm_Config()
	 * @model containment="true" required="true"
	 * @generated
	 */
	RLKConfig getConfig();

	/**
	 * Sets the value of the '{@link mderl.RLKAlgorithm#getConfig <em>Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Config</em>' containment reference.
	 * @see #getConfig()
	 * @generated
	 */
	void setConfig(RLKConfig value);

} // RLKAlgorithm
