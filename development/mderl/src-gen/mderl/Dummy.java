/**
 */
package mderl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dummy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mderl.MderlPackage#getDummy()
 * @model
 * @generated
 */
public interface Dummy extends general_elements {
} // Dummy
