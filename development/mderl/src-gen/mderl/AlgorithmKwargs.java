/**
 */
package mderl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Algorithm Kwargs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mderl.AlgorithmKwargs#getNum_epochs <em>Num epochs</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getNum_eval_steps <em>Num eval steps</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getNum_trains <em>Num trains</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getNum_expl_steps <em>Num expl steps</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getMin_num_steps <em>Min num steps</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getMax_path_length <em>Max path length</em>}</li>
 *   <li>{@link mderl.AlgorithmKwargs#getBatch_size <em>Batch size</em>}</li>
 * </ul>
 *
 * @see mderl.MderlPackage#getAlgorithmKwargs()
 * @model
 * @generated
 */
public interface AlgorithmKwargs extends EObject {
	/**
	 * Returns the value of the '<em><b>Num epochs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num epochs</em>' attribute.
	 * @see #setNum_epochs(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Num_epochs()
	 * @model required="true"
	 * @generated
	 */
	int getNum_epochs();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getNum_epochs <em>Num epochs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num epochs</em>' attribute.
	 * @see #getNum_epochs()
	 * @generated
	 */
	void setNum_epochs(int value);

	/**
	 * Returns the value of the '<em><b>Num eval steps</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num eval steps</em>' attribute.
	 * @see #setNum_eval_steps(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Num_eval_steps()
	 * @model default="1000" required="true"
	 * @generated
	 */
	int getNum_eval_steps();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getNum_eval_steps <em>Num eval steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num eval steps</em>' attribute.
	 * @see #getNum_eval_steps()
	 * @generated
	 */
	void setNum_eval_steps(int value);

	/**
	 * Returns the value of the '<em><b>Num trains</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num trains</em>' attribute.
	 * @see #setNum_trains(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Num_trains()
	 * @model default="1000" required="true"
	 * @generated
	 */
	int getNum_trains();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getNum_trains <em>Num trains</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num trains</em>' attribute.
	 * @see #getNum_trains()
	 * @generated
	 */
	void setNum_trains(int value);

	/**
	 * Returns the value of the '<em><b>Num expl steps</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num expl steps</em>' attribute.
	 * @see #setNum_expl_steps(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Num_expl_steps()
	 * @model default="1000" required="true"
	 * @generated
	 */
	int getNum_expl_steps();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getNum_expl_steps <em>Num expl steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num expl steps</em>' attribute.
	 * @see #getNum_expl_steps()
	 * @generated
	 */
	void setNum_expl_steps(int value);

	/**
	 * Returns the value of the '<em><b>Min num steps</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min num steps</em>' attribute.
	 * @see #setMin_num_steps(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Min_num_steps()
	 * @model default="1000" required="true"
	 * @generated
	 */
	int getMin_num_steps();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getMin_num_steps <em>Min num steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min num steps</em>' attribute.
	 * @see #getMin_num_steps()
	 * @generated
	 */
	void setMin_num_steps(int value);

	/**
	 * Returns the value of the '<em><b>Max path length</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max path length</em>' attribute.
	 * @see #setMax_path_length(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Max_path_length()
	 * @model default="1000" required="true"
	 * @generated
	 */
	int getMax_path_length();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getMax_path_length <em>Max path length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max path length</em>' attribute.
	 * @see #getMax_path_length()
	 * @generated
	 */
	void setMax_path_length(int value);

	/**
	 * Returns the value of the '<em><b>Batch size</b></em>' attribute.
	 * The default value is <code>"256"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Batch size</em>' attribute.
	 * @see #setBatch_size(int)
	 * @see mderl.MderlPackage#getAlgorithmKwargs_Batch_size()
	 * @model default="256" required="true"
	 * @generated
	 */
	int getBatch_size();

	/**
	 * Sets the value of the '{@link mderl.AlgorithmKwargs#getBatch_size <em>Batch size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Batch size</em>' attribute.
	 * @see #getBatch_size()
	 * @generated
	 */
	void setBatch_size(int value);

} // AlgorithmKwargs
