/**
 */
package mderl.impl;

import mderl.MderlPackage;
import mderl.TrainerKwargs;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trainer Kwargs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getDiscount <em>Discount</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getSoft_target_tau <em>Soft target tau</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getTarget_update_period <em>Target update period</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getPolicy_lr <em>Policy lr</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getQf_lr <em>Qf lr</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#getReward_scale <em>Reward scale</em>}</li>
 *   <li>{@link mderl.impl.TrainerKwargsImpl#isAuto_entropy_tuning <em>Auto entropy tuning</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TrainerKwargsImpl extends MinimalEObjectImpl.Container implements TrainerKwargs {
	/**
	 * The default value of the '{@link #getDiscount() <em>Discount</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscount()
	 * @generated
	 * @ordered
	 */
	protected static final float DISCOUNT_EDEFAULT = 0.99F;

	/**
	 * The cached value of the '{@link #getDiscount() <em>Discount</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscount()
	 * @generated
	 * @ordered
	 */
	protected float discount = DISCOUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSoft_target_tau() <em>Soft target tau</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoft_target_tau()
	 * @generated
	 * @ordered
	 */
	protected static final float SOFT_TARGET_TAU_EDEFAULT = 0.005F;

	/**
	 * The cached value of the '{@link #getSoft_target_tau() <em>Soft target tau</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoft_target_tau()
	 * @generated
	 * @ordered
	 */
	protected float soft_target_tau = SOFT_TARGET_TAU_EDEFAULT;

	/**
	 * The default value of the '{@link #getTarget_update_period() <em>Target update period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget_update_period()
	 * @generated
	 * @ordered
	 */
	protected static final int TARGET_UPDATE_PERIOD_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getTarget_update_period() <em>Target update period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget_update_period()
	 * @generated
	 * @ordered
	 */
	protected int target_update_period = TARGET_UPDATE_PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPolicy_lr() <em>Policy lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicy_lr()
	 * @generated
	 * @ordered
	 */
	protected static final float POLICY_LR_EDEFAULT = 3.0E-4F;

	/**
	 * The cached value of the '{@link #getPolicy_lr() <em>Policy lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicy_lr()
	 * @generated
	 * @ordered
	 */
	protected float policy_lr = POLICY_LR_EDEFAULT;

	/**
	 * The default value of the '{@link #getQf_lr() <em>Qf lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQf_lr()
	 * @generated
	 * @ordered
	 */
	protected static final float QF_LR_EDEFAULT = 3.0E-4F;

	/**
	 * The cached value of the '{@link #getQf_lr() <em>Qf lr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQf_lr()
	 * @generated
	 * @ordered
	 */
	protected float qf_lr = QF_LR_EDEFAULT;

	/**
	 * The default value of the '{@link #getReward_scale() <em>Reward scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReward_scale()
	 * @generated
	 * @ordered
	 */
	protected static final int REWARD_SCALE_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getReward_scale() <em>Reward scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReward_scale()
	 * @generated
	 * @ordered
	 */
	protected int reward_scale = REWARD_SCALE_EDEFAULT;

	/**
	 * The default value of the '{@link #isAuto_entropy_tuning() <em>Auto entropy tuning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAuto_entropy_tuning()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_ENTROPY_TUNING_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isAuto_entropy_tuning() <em>Auto entropy tuning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAuto_entropy_tuning()
	 * @generated
	 * @ordered
	 */
	protected boolean auto_entropy_tuning = AUTO_ENTROPY_TUNING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrainerKwargsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MderlPackage.Literals.TRAINER_KWARGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getDiscount() {
		return discount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDiscount(float newDiscount) {
		float oldDiscount = discount;
		discount = newDiscount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__DISCOUNT, oldDiscount,
					discount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getSoft_target_tau() {
		return soft_target_tau;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSoft_target_tau(float newSoft_target_tau) {
		float oldSoft_target_tau = soft_target_tau;
		soft_target_tau = newSoft_target_tau;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__SOFT_TARGET_TAU,
					oldSoft_target_tau, soft_target_tau));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getTarget_update_period() {
		return target_update_period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget_update_period(int newTarget_update_period) {
		int oldTarget_update_period = target_update_period;
		target_update_period = newTarget_update_period;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__TARGET_UPDATE_PERIOD,
					oldTarget_update_period, target_update_period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getPolicy_lr() {
		return policy_lr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPolicy_lr(float newPolicy_lr) {
		float oldPolicy_lr = policy_lr;
		policy_lr = newPolicy_lr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__POLICY_LR, oldPolicy_lr,
					policy_lr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getQf_lr() {
		return qf_lr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setQf_lr(float newQf_lr) {
		float oldQf_lr = qf_lr;
		qf_lr = newQf_lr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__QF_LR, oldQf_lr, qf_lr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getReward_scale() {
		return reward_scale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReward_scale(int newReward_scale) {
		int oldReward_scale = reward_scale;
		reward_scale = newReward_scale;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__REWARD_SCALE,
					oldReward_scale, reward_scale));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAuto_entropy_tuning() {
		return auto_entropy_tuning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAuto_entropy_tuning(boolean newAuto_entropy_tuning) {
		boolean oldAuto_entropy_tuning = auto_entropy_tuning;
		auto_entropy_tuning = newAuto_entropy_tuning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.TRAINER_KWARGS__AUTO_ENTROPY_TUNING,
					oldAuto_entropy_tuning, auto_entropy_tuning));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MderlPackage.TRAINER_KWARGS__DISCOUNT:
			return getDiscount();
		case MderlPackage.TRAINER_KWARGS__SOFT_TARGET_TAU:
			return getSoft_target_tau();
		case MderlPackage.TRAINER_KWARGS__TARGET_UPDATE_PERIOD:
			return getTarget_update_period();
		case MderlPackage.TRAINER_KWARGS__POLICY_LR:
			return getPolicy_lr();
		case MderlPackage.TRAINER_KWARGS__QF_LR:
			return getQf_lr();
		case MderlPackage.TRAINER_KWARGS__REWARD_SCALE:
			return getReward_scale();
		case MderlPackage.TRAINER_KWARGS__AUTO_ENTROPY_TUNING:
			return isAuto_entropy_tuning();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MderlPackage.TRAINER_KWARGS__DISCOUNT:
			setDiscount((Float) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__SOFT_TARGET_TAU:
			setSoft_target_tau((Float) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__TARGET_UPDATE_PERIOD:
			setTarget_update_period((Integer) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__POLICY_LR:
			setPolicy_lr((Float) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__QF_LR:
			setQf_lr((Float) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__REWARD_SCALE:
			setReward_scale((Integer) newValue);
			return;
		case MderlPackage.TRAINER_KWARGS__AUTO_ENTROPY_TUNING:
			setAuto_entropy_tuning((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MderlPackage.TRAINER_KWARGS__DISCOUNT:
			setDiscount(DISCOUNT_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__SOFT_TARGET_TAU:
			setSoft_target_tau(SOFT_TARGET_TAU_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__TARGET_UPDATE_PERIOD:
			setTarget_update_period(TARGET_UPDATE_PERIOD_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__POLICY_LR:
			setPolicy_lr(POLICY_LR_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__QF_LR:
			setQf_lr(QF_LR_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__REWARD_SCALE:
			setReward_scale(REWARD_SCALE_EDEFAULT);
			return;
		case MderlPackage.TRAINER_KWARGS__AUTO_ENTROPY_TUNING:
			setAuto_entropy_tuning(AUTO_ENTROPY_TUNING_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MderlPackage.TRAINER_KWARGS__DISCOUNT:
			return discount != DISCOUNT_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__SOFT_TARGET_TAU:
			return soft_target_tau != SOFT_TARGET_TAU_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__TARGET_UPDATE_PERIOD:
			return target_update_period != TARGET_UPDATE_PERIOD_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__POLICY_LR:
			return policy_lr != POLICY_LR_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__QF_LR:
			return qf_lr != QF_LR_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__REWARD_SCALE:
			return reward_scale != REWARD_SCALE_EDEFAULT;
		case MderlPackage.TRAINER_KWARGS__AUTO_ENTROPY_TUNING:
			return auto_entropy_tuning != AUTO_ENTROPY_TUNING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (discount: ");
		result.append(discount);
		result.append(", soft_target_tau: ");
		result.append(soft_target_tau);
		result.append(", target_update_period: ");
		result.append(target_update_period);
		result.append(", policy_lr: ");
		result.append(policy_lr);
		result.append(", qf_lr: ");
		result.append(qf_lr);
		result.append(", reward_scale: ");
		result.append(reward_scale);
		result.append(", auto_entropy_tuning: ");
		result.append(auto_entropy_tuning);
		result.append(')');
		return result.toString();
	}

} //TrainerKwargsImpl
