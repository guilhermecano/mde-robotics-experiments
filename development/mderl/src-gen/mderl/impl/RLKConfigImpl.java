/**
 */
package mderl.impl;

import java.util.Collection;

import mderl.AlgorithmKwargs;
import mderl.MderlPackage;
import mderl.RLKConfig;
import mderl.TrainerKwargs;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RLK Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mderl.impl.RLKConfigImpl#getAlgorithm <em>Algorithm</em>}</li>
 *   <li>{@link mderl.impl.RLKConfigImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link mderl.impl.RLKConfigImpl#getLayer_size <em>Layer size</em>}</li>
 *   <li>{@link mderl.impl.RLKConfigImpl#getReplay_buffer_size <em>Replay buffer size</em>}</li>
 *   <li>{@link mderl.impl.RLKConfigImpl#getTrainerkwargs <em>Trainerkwargs</em>}</li>
 *   <li>{@link mderl.impl.RLKConfigImpl#getAlgorithmkwargs <em>Algorithmkwargs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RLKConfigImpl extends MinimalEObjectImpl.Container implements RLKConfig {
	/**
	 * The default value of the '{@link #getAlgorithm() <em>Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlgorithm()
	 * @generated
	 * @ordered
	 */
	protected static final String ALGORITHM_EDEFAULT = "SAC";

	/**
	 * The cached value of the '{@link #getAlgorithm() <em>Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlgorithm()
	 * @generated
	 * @ordered
	 */
	protected String algorithm = ALGORITHM_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "normal";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLayer_size() <em>Layer size</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayer_size()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> layer_size;

	/**
	 * The default value of the '{@link #getReplay_buffer_size() <em>Replay buffer size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReplay_buffer_size()
	 * @generated
	 * @ordered
	 */
	protected static final int REPLAY_BUFFER_SIZE_EDEFAULT = 1000000;

	/**
	 * The cached value of the '{@link #getReplay_buffer_size() <em>Replay buffer size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReplay_buffer_size()
	 * @generated
	 * @ordered
	 */
	protected int replay_buffer_size = REPLAY_BUFFER_SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTrainerkwargs() <em>Trainerkwargs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrainerkwargs()
	 * @generated
	 * @ordered
	 */
	protected TrainerKwargs trainerkwargs;

	/**
	 * The cached value of the '{@link #getAlgorithmkwargs() <em>Algorithmkwargs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlgorithmkwargs()
	 * @generated
	 * @ordered
	 */
	protected AlgorithmKwargs algorithmkwargs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RLKConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MderlPackage.Literals.RLK_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAlgorithm() {
		return algorithm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAlgorithm(String newAlgorithm) {
		String oldAlgorithm = algorithm;
		algorithm = newAlgorithm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.RLK_CONFIG__ALGORITHM, oldAlgorithm,
					algorithm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.RLK_CONFIG__VERSION, oldVersion,
					version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Integer> getLayer_size() {
		if (layer_size == null) {
			layer_size = new EDataTypeEList<Integer>(Integer.class, this, MderlPackage.RLK_CONFIG__LAYER_SIZE);
		}
		return layer_size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getReplay_buffer_size() {
		return replay_buffer_size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReplay_buffer_size(int newReplay_buffer_size) {
		int oldReplay_buffer_size = replay_buffer_size;
		replay_buffer_size = newReplay_buffer_size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.RLK_CONFIG__REPLAY_BUFFER_SIZE,
					oldReplay_buffer_size, replay_buffer_size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TrainerKwargs getTrainerkwargs() {
		return trainerkwargs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTrainerkwargs(TrainerKwargs newTrainerkwargs, NotificationChain msgs) {
		TrainerKwargs oldTrainerkwargs = trainerkwargs;
		trainerkwargs = newTrainerkwargs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MderlPackage.RLK_CONFIG__TRAINERKWARGS, oldTrainerkwargs, newTrainerkwargs);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTrainerkwargs(TrainerKwargs newTrainerkwargs) {
		if (newTrainerkwargs != trainerkwargs) {
			NotificationChain msgs = null;
			if (trainerkwargs != null)
				msgs = ((InternalEObject) trainerkwargs).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MderlPackage.RLK_CONFIG__TRAINERKWARGS, null, msgs);
			if (newTrainerkwargs != null)
				msgs = ((InternalEObject) newTrainerkwargs).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MderlPackage.RLK_CONFIG__TRAINERKWARGS, null, msgs);
			msgs = basicSetTrainerkwargs(newTrainerkwargs, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.RLK_CONFIG__TRAINERKWARGS,
					newTrainerkwargs, newTrainerkwargs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AlgorithmKwargs getAlgorithmkwargs() {
		return algorithmkwargs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlgorithmkwargs(AlgorithmKwargs newAlgorithmkwargs, NotificationChain msgs) {
		AlgorithmKwargs oldAlgorithmkwargs = algorithmkwargs;
		algorithmkwargs = newAlgorithmkwargs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MderlPackage.RLK_CONFIG__ALGORITHMKWARGS, oldAlgorithmkwargs, newAlgorithmkwargs);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAlgorithmkwargs(AlgorithmKwargs newAlgorithmkwargs) {
		if (newAlgorithmkwargs != algorithmkwargs) {
			NotificationChain msgs = null;
			if (algorithmkwargs != null)
				msgs = ((InternalEObject) algorithmkwargs).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MderlPackage.RLK_CONFIG__ALGORITHMKWARGS, null, msgs);
			if (newAlgorithmkwargs != null)
				msgs = ((InternalEObject) newAlgorithmkwargs).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MderlPackage.RLK_CONFIG__ALGORITHMKWARGS, null, msgs);
			msgs = basicSetAlgorithmkwargs(newAlgorithmkwargs, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.RLK_CONFIG__ALGORITHMKWARGS,
					newAlgorithmkwargs, newAlgorithmkwargs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MderlPackage.RLK_CONFIG__TRAINERKWARGS:
			return basicSetTrainerkwargs(null, msgs);
		case MderlPackage.RLK_CONFIG__ALGORITHMKWARGS:
			return basicSetAlgorithmkwargs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MderlPackage.RLK_CONFIG__ALGORITHM:
			return getAlgorithm();
		case MderlPackage.RLK_CONFIG__VERSION:
			return getVersion();
		case MderlPackage.RLK_CONFIG__LAYER_SIZE:
			return getLayer_size();
		case MderlPackage.RLK_CONFIG__REPLAY_BUFFER_SIZE:
			return getReplay_buffer_size();
		case MderlPackage.RLK_CONFIG__TRAINERKWARGS:
			return getTrainerkwargs();
		case MderlPackage.RLK_CONFIG__ALGORITHMKWARGS:
			return getAlgorithmkwargs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MderlPackage.RLK_CONFIG__ALGORITHM:
			setAlgorithm((String) newValue);
			return;
		case MderlPackage.RLK_CONFIG__VERSION:
			setVersion((String) newValue);
			return;
		case MderlPackage.RLK_CONFIG__LAYER_SIZE:
			getLayer_size().clear();
			getLayer_size().addAll((Collection<? extends Integer>) newValue);
			return;
		case MderlPackage.RLK_CONFIG__REPLAY_BUFFER_SIZE:
			setReplay_buffer_size((Integer) newValue);
			return;
		case MderlPackage.RLK_CONFIG__TRAINERKWARGS:
			setTrainerkwargs((TrainerKwargs) newValue);
			return;
		case MderlPackage.RLK_CONFIG__ALGORITHMKWARGS:
			setAlgorithmkwargs((AlgorithmKwargs) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MderlPackage.RLK_CONFIG__ALGORITHM:
			setAlgorithm(ALGORITHM_EDEFAULT);
			return;
		case MderlPackage.RLK_CONFIG__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case MderlPackage.RLK_CONFIG__LAYER_SIZE:
			getLayer_size().clear();
			return;
		case MderlPackage.RLK_CONFIG__REPLAY_BUFFER_SIZE:
			setReplay_buffer_size(REPLAY_BUFFER_SIZE_EDEFAULT);
			return;
		case MderlPackage.RLK_CONFIG__TRAINERKWARGS:
			setTrainerkwargs((TrainerKwargs) null);
			return;
		case MderlPackage.RLK_CONFIG__ALGORITHMKWARGS:
			setAlgorithmkwargs((AlgorithmKwargs) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MderlPackage.RLK_CONFIG__ALGORITHM:
			return ALGORITHM_EDEFAULT == null ? algorithm != null : !ALGORITHM_EDEFAULT.equals(algorithm);
		case MderlPackage.RLK_CONFIG__VERSION:
			return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		case MderlPackage.RLK_CONFIG__LAYER_SIZE:
			return layer_size != null && !layer_size.isEmpty();
		case MderlPackage.RLK_CONFIG__REPLAY_BUFFER_SIZE:
			return replay_buffer_size != REPLAY_BUFFER_SIZE_EDEFAULT;
		case MderlPackage.RLK_CONFIG__TRAINERKWARGS:
			return trainerkwargs != null;
		case MderlPackage.RLK_CONFIG__ALGORITHMKWARGS:
			return algorithmkwargs != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (algorithm: ");
		result.append(algorithm);
		result.append(", version: ");
		result.append(version);
		result.append(", layer_size: ");
		result.append(layer_size);
		result.append(", replay_buffer_size: ");
		result.append(replay_buffer_size);
		result.append(')');
		return result.toString();
	}

} //RLKConfigImpl
