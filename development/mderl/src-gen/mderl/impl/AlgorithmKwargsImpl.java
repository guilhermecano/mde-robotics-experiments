/**
 */
package mderl.impl;

import mderl.AlgorithmKwargs;
import mderl.MderlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Algorithm Kwargs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getNum_epochs <em>Num epochs</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getNum_eval_steps <em>Num eval steps</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getNum_trains <em>Num trains</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getNum_expl_steps <em>Num expl steps</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getMin_num_steps <em>Min num steps</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getMax_path_length <em>Max path length</em>}</li>
 *   <li>{@link mderl.impl.AlgorithmKwargsImpl#getBatch_size <em>Batch size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlgorithmKwargsImpl extends MinimalEObjectImpl.Container implements AlgorithmKwargs {
	/**
	 * The default value of the '{@link #getNum_epochs() <em>Num epochs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_epochs()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_EPOCHS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNum_epochs() <em>Num epochs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_epochs()
	 * @generated
	 * @ordered
	 */
	protected int num_epochs = NUM_EPOCHS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNum_eval_steps() <em>Num eval steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_eval_steps()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_EVAL_STEPS_EDEFAULT = 1000;

	/**
	 * The cached value of the '{@link #getNum_eval_steps() <em>Num eval steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_eval_steps()
	 * @generated
	 * @ordered
	 */
	protected int num_eval_steps = NUM_EVAL_STEPS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNum_trains() <em>Num trains</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_trains()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_TRAINS_EDEFAULT = 1000;

	/**
	 * The cached value of the '{@link #getNum_trains() <em>Num trains</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_trains()
	 * @generated
	 * @ordered
	 */
	protected int num_trains = NUM_TRAINS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNum_expl_steps() <em>Num expl steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_expl_steps()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_EXPL_STEPS_EDEFAULT = 1000;

	/**
	 * The cached value of the '{@link #getNum_expl_steps() <em>Num expl steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_expl_steps()
	 * @generated
	 * @ordered
	 */
	protected int num_expl_steps = NUM_EXPL_STEPS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_num_steps() <em>Min num steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_num_steps()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_NUM_STEPS_EDEFAULT = 1000;

	/**
	 * The cached value of the '{@link #getMin_num_steps() <em>Min num steps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_num_steps()
	 * @generated
	 * @ordered
	 */
	protected int min_num_steps = MIN_NUM_STEPS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_path_length() <em>Max path length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_path_length()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_PATH_LENGTH_EDEFAULT = 1000;

	/**
	 * The cached value of the '{@link #getMax_path_length() <em>Max path length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_path_length()
	 * @generated
	 * @ordered
	 */
	protected int max_path_length = MAX_PATH_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getBatch_size() <em>Batch size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatch_size()
	 * @generated
	 * @ordered
	 */
	protected static final int BATCH_SIZE_EDEFAULT = 256;

	/**
	 * The cached value of the '{@link #getBatch_size() <em>Batch size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatch_size()
	 * @generated
	 * @ordered
	 */
	protected int batch_size = BATCH_SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlgorithmKwargsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MderlPackage.Literals.ALGORITHM_KWARGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNum_epochs() {
		return num_epochs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNum_epochs(int newNum_epochs) {
		int oldNum_epochs = num_epochs;
		num_epochs = newNum_epochs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__NUM_EPOCHS,
					oldNum_epochs, num_epochs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNum_eval_steps() {
		return num_eval_steps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNum_eval_steps(int newNum_eval_steps) {
		int oldNum_eval_steps = num_eval_steps;
		num_eval_steps = newNum_eval_steps;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__NUM_EVAL_STEPS,
					oldNum_eval_steps, num_eval_steps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNum_trains() {
		return num_trains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNum_trains(int newNum_trains) {
		int oldNum_trains = num_trains;
		num_trains = newNum_trains;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__NUM_TRAINS,
					oldNum_trains, num_trains));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNum_expl_steps() {
		return num_expl_steps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNum_expl_steps(int newNum_expl_steps) {
		int oldNum_expl_steps = num_expl_steps;
		num_expl_steps = newNum_expl_steps;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__NUM_EXPL_STEPS,
					oldNum_expl_steps, num_expl_steps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMin_num_steps() {
		return min_num_steps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMin_num_steps(int newMin_num_steps) {
		int oldMin_num_steps = min_num_steps;
		min_num_steps = newMin_num_steps;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__MIN_NUM_STEPS,
					oldMin_num_steps, min_num_steps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMax_path_length() {
		return max_path_length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMax_path_length(int newMax_path_length) {
		int oldMax_path_length = max_path_length;
		max_path_length = newMax_path_length;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__MAX_PATH_LENGTH,
					oldMax_path_length, max_path_length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getBatch_size() {
		return batch_size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBatch_size(int newBatch_size) {
		int oldBatch_size = batch_size;
		batch_size = newBatch_size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MderlPackage.ALGORITHM_KWARGS__BATCH_SIZE,
					oldBatch_size, batch_size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MderlPackage.ALGORITHM_KWARGS__NUM_EPOCHS:
			return getNum_epochs();
		case MderlPackage.ALGORITHM_KWARGS__NUM_EVAL_STEPS:
			return getNum_eval_steps();
		case MderlPackage.ALGORITHM_KWARGS__NUM_TRAINS:
			return getNum_trains();
		case MderlPackage.ALGORITHM_KWARGS__NUM_EXPL_STEPS:
			return getNum_expl_steps();
		case MderlPackage.ALGORITHM_KWARGS__MIN_NUM_STEPS:
			return getMin_num_steps();
		case MderlPackage.ALGORITHM_KWARGS__MAX_PATH_LENGTH:
			return getMax_path_length();
		case MderlPackage.ALGORITHM_KWARGS__BATCH_SIZE:
			return getBatch_size();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MderlPackage.ALGORITHM_KWARGS__NUM_EPOCHS:
			setNum_epochs((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EVAL_STEPS:
			setNum_eval_steps((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_TRAINS:
			setNum_trains((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EXPL_STEPS:
			setNum_expl_steps((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__MIN_NUM_STEPS:
			setMin_num_steps((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__MAX_PATH_LENGTH:
			setMax_path_length((Integer) newValue);
			return;
		case MderlPackage.ALGORITHM_KWARGS__BATCH_SIZE:
			setBatch_size((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MderlPackage.ALGORITHM_KWARGS__NUM_EPOCHS:
			setNum_epochs(NUM_EPOCHS_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EVAL_STEPS:
			setNum_eval_steps(NUM_EVAL_STEPS_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_TRAINS:
			setNum_trains(NUM_TRAINS_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EXPL_STEPS:
			setNum_expl_steps(NUM_EXPL_STEPS_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__MIN_NUM_STEPS:
			setMin_num_steps(MIN_NUM_STEPS_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__MAX_PATH_LENGTH:
			setMax_path_length(MAX_PATH_LENGTH_EDEFAULT);
			return;
		case MderlPackage.ALGORITHM_KWARGS__BATCH_SIZE:
			setBatch_size(BATCH_SIZE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MderlPackage.ALGORITHM_KWARGS__NUM_EPOCHS:
			return num_epochs != NUM_EPOCHS_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EVAL_STEPS:
			return num_eval_steps != NUM_EVAL_STEPS_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__NUM_TRAINS:
			return num_trains != NUM_TRAINS_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__NUM_EXPL_STEPS:
			return num_expl_steps != NUM_EXPL_STEPS_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__MIN_NUM_STEPS:
			return min_num_steps != MIN_NUM_STEPS_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__MAX_PATH_LENGTH:
			return max_path_length != MAX_PATH_LENGTH_EDEFAULT;
		case MderlPackage.ALGORITHM_KWARGS__BATCH_SIZE:
			return batch_size != BATCH_SIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (num_epochs: ");
		result.append(num_epochs);
		result.append(", num_eval_steps: ");
		result.append(num_eval_steps);
		result.append(", num_trains: ");
		result.append(num_trains);
		result.append(", num_expl_steps: ");
		result.append(num_expl_steps);
		result.append(", min_num_steps: ");
		result.append(min_num_steps);
		result.append(", max_path_length: ");
		result.append(max_path_length);
		result.append(", batch_size: ");
		result.append(batch_size);
		result.append(')');
		return result.toString();
	}

} //AlgorithmKwargsImpl
