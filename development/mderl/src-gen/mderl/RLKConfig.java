/**
 */
package mderl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RLK Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mderl.RLKConfig#getAlgorithm <em>Algorithm</em>}</li>
 *   <li>{@link mderl.RLKConfig#getVersion <em>Version</em>}</li>
 *   <li>{@link mderl.RLKConfig#getLayer_size <em>Layer size</em>}</li>
 *   <li>{@link mderl.RLKConfig#getReplay_buffer_size <em>Replay buffer size</em>}</li>
 *   <li>{@link mderl.RLKConfig#getTrainerkwargs <em>Trainerkwargs</em>}</li>
 *   <li>{@link mderl.RLKConfig#getAlgorithmkwargs <em>Algorithmkwargs</em>}</li>
 * </ul>
 *
 * @see mderl.MderlPackage#getRLKConfig()
 * @model
 * @generated
 */
public interface RLKConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Algorithm</b></em>' attribute.
	 * The default value is <code>"SAC"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Algorithm</em>' attribute.
	 * @see #setAlgorithm(String)
	 * @see mderl.MderlPackage#getRLKConfig_Algorithm()
	 * @model default="SAC" required="true"
	 * @generated
	 */
	String getAlgorithm();

	/**
	 * Sets the value of the '{@link mderl.RLKConfig#getAlgorithm <em>Algorithm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Algorithm</em>' attribute.
	 * @see #getAlgorithm()
	 * @generated
	 */
	void setAlgorithm(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * The default value is <code>"normal"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see mderl.MderlPackage#getRLKConfig_Version()
	 * @model default="normal" required="true"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link mderl.RLKConfig#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Layer size</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layer size</em>' attribute list.
	 * @see mderl.MderlPackage#getRLKConfig_Layer_size()
	 * @model default="64" unique="false" required="true"
	 * @generated
	 */
	EList<Integer> getLayer_size();

	/**
	 * Returns the value of the '<em><b>Replay buffer size</b></em>' attribute.
	 * The default value is <code>"1000000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Replay buffer size</em>' attribute.
	 * @see #setReplay_buffer_size(int)
	 * @see mderl.MderlPackage#getRLKConfig_Replay_buffer_size()
	 * @model default="1000000"
	 * @generated
	 */
	int getReplay_buffer_size();

	/**
	 * Sets the value of the '{@link mderl.RLKConfig#getReplay_buffer_size <em>Replay buffer size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Replay buffer size</em>' attribute.
	 * @see #getReplay_buffer_size()
	 * @generated
	 */
	void setReplay_buffer_size(int value);

	/**
	 * Returns the value of the '<em><b>Trainerkwargs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trainerkwargs</em>' containment reference.
	 * @see #setTrainerkwargs(TrainerKwargs)
	 * @see mderl.MderlPackage#getRLKConfig_Trainerkwargs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TrainerKwargs getTrainerkwargs();

	/**
	 * Sets the value of the '{@link mderl.RLKConfig#getTrainerkwargs <em>Trainerkwargs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trainerkwargs</em>' containment reference.
	 * @see #getTrainerkwargs()
	 * @generated
	 */
	void setTrainerkwargs(TrainerKwargs value);

	/**
	 * Returns the value of the '<em><b>Algorithmkwargs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Algorithmkwargs</em>' containment reference.
	 * @see #setAlgorithmkwargs(AlgorithmKwargs)
	 * @see mderl.MderlPackage#getRLKConfig_Algorithmkwargs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AlgorithmKwargs getAlgorithmkwargs();

	/**
	 * Sets the value of the '{@link mderl.RLKConfig#getAlgorithmkwargs <em>Algorithmkwargs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Algorithmkwargs</em>' containment reference.
	 * @see #getAlgorithmkwargs()
	 * @generated
	 */
	void setAlgorithmkwargs(AlgorithmKwargs value);

} // RLKConfig
