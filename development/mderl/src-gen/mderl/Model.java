/**
 */
package mderl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mderl.MderlPackage#getModel()
 * @model abstract="true"
 * @generated
 */
public interface Model extends EObject {
} // Model
