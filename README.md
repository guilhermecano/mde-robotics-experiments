# MDE Robotics Experiments

Final Project for **MO631A - Model Driven Engineering**: Meta Model for Reinforcement learning experiments in robotics using V-REP, PyRep and RLKit.

## Overview:

This project contains
1. The definition of a ![meta-model](https://gitlab.com/guilhermecano/mde-robotics-experiments/blob/master/assets/final_mde.png) for building Reinforcement Learning Experiments with a set of robots in V-REP (Current CoppeliaSim), defined with Eclipse Modelling Framework (EMF).

2. An Acceleo project for generating the required python scripts (Model-to-text) in order to run different sets of experiments. These files were developed to be used along the larocs_sim package (under development), that can be found at:

    https://gitlab.com/LaRoCS/larocs_sim

3. A Domain Specific Language (DSL) for defining reinforcement learning experiences and that can be also used along with Acceleo for code generation. From .exp files containing the experiment setup, it is possible to recover a model that conforms to the meta-model in the mderl project. 

Current Robots:
    1. Parrot ArDrone (aerial)
    2. TurtleRobot (mobile/wheeled)
    3. Panda (fixed/arm)

Support for RLKit models:
    1. Soft Actor Critic

## Limitations

Currently, it is only possible to define one agent in an experiment, even though more than one robot can exist in the environment. There are some work to be done at larocs_sim in order to allow a collection of agents in the meta-model.


